pub type Pos = (usize, usize);

#[derive(Debug)]
pub enum Var<'a> {
  Simple(&'a str),
  Field(Box<Var<'a>>, &'a str),
  Subscript(Box<Var<'a>>, Box<Exp<'a>>),
}

#[derive(Debug)]
pub enum Exp<'a> {
  Var(Box<Var<'a>>),
  Nil,
  Int(i32),
  Str(&'a str, Pos),
  Call(&'a str, Vec<Box<Exp<'a>>>, Pos),
  Bin(BinOp, Box<Exp<'a>>, Box<Exp<'a>>, Pos),
  Un(UnOp, Box<Exp<'a>>, Pos),
  Record(Vec<(&'a str, Box<Exp<'a>>)>, &'a str, Pos), // fields, constructor
  Seq(Vec<(Box<Exp<'a>>, Pos)>),
  Assign(Box<Var<'a>>, Box<Exp<'a>>, Pos),
  If(Box<Exp<'a>>, Box<Exp<'a>>, Box<Exp<'a>>, Pos), // cond, then, else
  While(Box<Exp<'a>>, Box<Exp<'a>>, Pos), // cond, body
  For(&'a str, Box<Exp<'a>>, Box<Exp<'a>>, Box<Exp<'a>>, Pos), // var, lo, hi, body
  Break(Pos),
  Let(Vec<Box<Dec<'a>>>, Box<Exp<'a>>, Pos), // decs, body
  Array(&'a str, Box<Exp<'a>>, Box<Exp<'a>>, Pos), // type, size, init
}

#[derive(Debug)]
pub enum Dec<'a> {
  Fun(&'a str, Vec<Field<'a>>, Option<(&'a str, Pos)>, Box<Exp<'a>>, Pos), // name, params, type, body
  Var(&'a str, Option<(&'a str, Pos)>, Box<Exp<'a>>, Pos), // name, type, init, pos
  Type(&'a str, Type<'a>, Pos), // name, type
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Type<'a> {
  Name(&'a str, Pos),
  Array(&'a str, Pos),
  Record(Vec<Box<Field<'a>>>, Pos),
}

#[derive(Debug)]
pub enum UnOp {
  Minus,
}

#[derive(Debug)]
pub enum BinOp {
  Mul, Div, Add, Sub, Lt, Le, Gt, Ge, Equ, And, Or,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Field<'a>(pub &'a str, pub &'a str); // name, type
