use rpds::{RedBlackTreeMap, Vector};
use std::rc::Rc;

use ast::{BinOp, Dec, Pos, Type, Var};
use ast;
use ir;
use translate::{Access, Level, Translator, Frag, simple_var, cnst, exp, eseq_, call, trans_break, stms, mul};
use translate;
use temp::Label;

type VEnv<'a> = RedBlackTreeMap<&'a str, Box<EnvEntry<'a>>>;
type TEnv<'a> = RedBlackTreeMap<&'a str, Type<'a>>;
type ExpTy<'a> = (Box<translate::Exp<'a>>, Type<'a>);

const NO_POS: ast::Pos = (0, 0);
const INT: Type = Type::Name("int", NO_POS);
const STRING: Type = Type::Name("string", NO_POS);
const BOOL: Type = Type::Name("bool", NO_POS);
const UNIT: Type = Type::Name("unit", NO_POS);

#[derive(Clone, Debug)]
struct Env<'a> {
  venv: VEnv<'a>,
  tenv: TEnv<'a>,
  level: Rc<Level<'a>>,
  trans: Translator<'a>,
}

#[derive(Debug)]
pub enum EnvEntry<'a> {
  Var { access: Access, ty: Type<'a> },
  Fun { level: Rc<Level<'a>>, label: Label<'a>, formals: Vec<Type<'a>>, result: Type<'a> }
}

pub fn run<'a>(s: &'a Box<ast::Exp<'a>>) -> Result<Vector<Frag<'a>>, String> {
  let level = Level::outermost();
  let mut trans = Translator::new();
  let venv = RedBlackTreeMap::new()
    .insert("print", Box::new(EnvEntry::Fun {
      level: level.clone(),
      label: trans.create_named_label("print"),
      formals: vector1(STRING),
      result: UNIT
    }));
  let tenv = RedBlackTreeMap::new()
    .insert("int", INT)
    .insert("string", STRING)
    .insert("bool", BOOL)
    .insert("unit", UNIT);
  let e = Env { tenv, venv, level, trans };
  let (e, (exp, _)) = e.trans_exp(s)?;
  let mut trans = e.trans.clone();
  trans.return_value(exp, e.level);
  Ok(trans.get_fragments().clone())
}

impl <'a> Env<'a> {
  fn trans_stm(&self, s: &Box<ast::Exp<'a>>) -> Result<(Env<'a>, Box<ir::Stm<'a>>), String> {
    match s.as_ref() {
      ast::Exp::Var(_) | ast::Exp::Nil | ast::Exp::Int(_) | ast::Exp::Str(_, _) | ast::Exp::Call(_, _, _) |
      ast::Exp::Bin(_, _, _, _) | ast::Exp::Un(_, _, _) | ast::Exp::Record(_, _, _) | ast::Exp::Let(_, _, _) =>
        self.trans_stm_thru_exp(s),
      ast::Exp::Seq(ss) => self.trans_stms(ss),
      ast::Exp::Assign(l, r, _) => {
        let (e, (tm2, ty2)) = self.trans_exp(r)?;
        let (e, (tm1, ty1)) = e.trans_var(l)?;
        if ty1 != ty2 {
          return Err(format!("Type mismatch: {:?} {:?}", ty1, ty2))
        }
        let mut trans = e.trans.clone();
        let s = trans.trans_assign(tm1, tm2);
        Ok((e.set_trans(trans), s))
      }
      ast::Exp::If(c, t, f, _) => {
        let (e, (c, ty)) = self.trans_exp(c)?;
        check_types(&ty, &INT)?;
        let (e1, t) = e.trans_stm(t)?;
        let (e2, f) = e.set_trans(e1.trans).trans_stm(f)?;
        let mut trans = e2.trans.clone();
        let s = trans.trans_if_then_else(c, t, f);
        Ok((self.set_trans(trans), s))
      }
      ast::Exp::While(c, s, _) => {
        let (e, (_, ty)) = self.trans_exp(c)?;
        check_types(&ty, &BOOL)?;
        let (e, s) = e.trans_stm(s)?;
        Ok((self.set_trans(e.trans), s))
      }
      ast::Exp::For(i, beg, end, s, _) => {
        let (e, beg, end) = self.check_both_ints(beg, end)?;
        let (e, s) = e.trans_for(i, beg, end, s)?;
        Ok((self.set_trans(e.trans), s))
      }
      ast::Exp::Break(_) => {
        let (s, level) = trans_break(self.level.clone())?;
        Ok((self.set_level(level), s))
      }
      ast::Exp::Array(ty, cnt, _, _) => {
        let (_, size) = find_type_and_size(ty, &self.tenv)?;
        let (e, (cnt, _)) = self.trans_exp(cnt)?;
        let mut trans = e.trans.clone();
        let cnt = trans.un_ex(cnt);
        let s = trans.un_nx(translate::malloc(mul(cnst(size as i32), cnt)));
        Ok((e.set_trans(trans), s))
      }
    }
  }

  fn trans_stm_thru_exp(&self, tm: &Box<ast::Exp<'a>>) -> Result<(Env<'a>, Box<ir::Stm<'a>>), String> {
    let (e, (tm, _)) = self.trans_exp(tm)?;
    e.un_ux(tm)
  }

  fn trans_exp(&self, tm: &Box<ast::Exp<'a>>) -> Result<(Env<'a>, ExpTy<'a>), String> {
    match tm.as_ref() {
      ast::Exp::Var(v) => self.trans_var(v),
      ast::Exp::Nil => unimplemented!(),
      ast::Exp::Int(i) => self.trans_int(i),
      ast::Exp::Str(s, _) => self.trans_str(s),
      ast::Exp::Call(n, vs, _) => self.trans_call(n, vs),
      ast::Exp::Bin(op, l, r, p) => self.trans_bin(op, l, r, p),
      ast::Exp::Un(_, _, _) => unimplemented!(),
      ast::Exp::Record(fs, c, _) => self.trans_record(fs, c),
      ast::Exp::Seq(es) => self.trans_stms_to_exp(es),
      ast::Exp::Let(ds, exp, _) => self.trans_let(ds, exp),
      _ => Err(format!("Unsupported expression: {:?}", tm)),
    }
  }

  fn un_ux(&self, tm: Box<translate::Exp<'a>>) -> Result<(Env<'a>, Box<ir::Stm<'a>>), String> {
    let mut trans = self.trans.clone();
    let s = trans.un_nx(tm);
    Ok((self.set_trans(trans), s))
  }

  fn trans_for(&self, i: &'a str, beg: Box<translate::Exp<'a>>, end: Box<translate::Exp<'a>>,  s: &Box<ast::Exp<'a>>)
      -> Result<(Env<'a>, Box<ir::Stm<'a>>), String> {
    let (_, access) = self.level.alloc_local();
    let venv = self.venv.insert(i, Box::new(EnvEntry::Var { access: access.clone(), ty: INT }));
    let (e, s) = self.set_venv(venv).trans_stm(s)?;
    let mut trans = e.trans.clone();
    let s = trans.trans_for(self.level.clone(), access, beg, end, s)?;
    Ok((self.set_trans(trans), s))
  }

  fn trans_fun(&self, i: &'a str, ps: &Vec<ast::Field<'a>>, r: &Option<(&'a str, Pos)>, s: &Box<ast::Exp<'a>>)
      -> Result<Env<'a>, String> {
    let mut tys = Vec::new();
    for ast::Field(_, i) in ps.into_iter() {
      let ty = self.tenv.get(i).ok_or_else(|| format!("No type: {}", i))?;
      tys.push(ty.clone());
    }
    let level = Level::new(i, ps.len(), self.level.clone());
    let (e, (tm, ty1)) = self.set_level(level.clone()).trans_exp(s)?;
    if let Some((tn, _)) = r {
      match self.tenv.get(tn) {
        Some(ty2) =>
          if &ty1 != ty2 {
            return Err(format!("Type mismatch: {:?} {:?}", ty1, ty2))
          }
        None => return Err(format!("No type: {}", tn))
      }
    }
    let mut trans = e.trans.clone();
    let label = trans.trans_fun(i, tm, e.level.get_frame());
    let f = Box::new(EnvEntry::Fun { level, label, formals: tys, result: UNIT });
    let venv = self.venv.insert(i, f);
    Ok(self.set_trans(trans).set_venv(venv))
  }

  fn trans_stms(&self, ss: &Vec<(Box<ast::Exp<'a>>, Pos)>) -> Result<(Env<'a>, Box<ir::Stm<'a>>), String> {
    let mut iss = Vec::new();
    let mut e = self.clone();
    for (d, _) in ss.into_iter() {
      let (e1, is) = e.trans_stm(d)?;
      iss.push(is);
      e = e1;
    }
    let s = stms(iss)?;
    Ok((e, s))
  }

  fn trans_stms_to_exp(&self, ss: &Vec<(Box<ast::Exp<'a>>, Pos)>) -> Result<(Env<'a>, ExpTy<'a>), String> {
    if ss.len() == 0 {
      return Err(String::from("No body in statements"))
    }
    let (ref tm, _) = ss[ss.len()-1];
    let mut iss = Vec::new();
    let mut e = self.clone();
    for (d, _) in ss[0..ss.len()-1].into_iter() {
      let (e1, is) = e.trans_stm(d)?;
      iss.push(is);
      e = e1;
    }
    let s = stms(iss)?;
    let (e, (tm, ty)) = e.trans_exp(&tm)?;
    let mut trans = e.trans.clone();
    let tm = trans.un_ex(tm);
    Ok((e.set_trans(trans), (eseq_(s, tm), ty)))
  }

  fn trans_decs(&self, ds: &Vec<Box<Dec<'a>>>) -> Result<Env<'a>, String> {
    let mut e = self.clone();
    for d in ds.into_iter() {
      let e1 = e.trans_dec(d)?;
      e = e1;
    }
    Ok(e)
  }

  fn trans_dec(&self, d: &Box<Dec<'a>>) -> Result<Env<'a>, String> {
    match d.as_ref() {
      Dec::Var(i, None, e, _) => self.trans_vardec(i, e),
      Dec::Var(i, Some((ty, _)), e, _) => self.trans_vardec_type(ty, i, e),
      Dec::Fun(i, ps, o, e, _) => self.trans_fun(i, ps, o, e),
      Dec::Type(i, ty, _) => 
        Ok(self.set_tenv(self.tenv.insert(i, ty.clone()))),
    }
  }

  fn trans_vardec(&self, n: &'a str, tm: &Box<ast::Exp<'a>>) -> Result<Env<'a>, String> {
    let (e, (_, ty)) = self.trans_exp(tm)?;
    let (_, access) = e.level.alloc_local();
    let venv = e.venv.insert(n, Box::new(EnvEntry::Var { access, ty: ty.clone() }));
    Ok(e.set_venv(venv))
  }

  fn trans_vardec_type(&self, ty: &'a str, n: &'a str, tm: &Box<ast::Exp<'a>>)
      -> Result<Env<'a>, String> {
    let ty = self.tenv.get(ty).ok_or_else(|| format!("No type: {}", ty))?;
    match self.venv.get(n).map(|x| x.as_ref()) {
      Some(EnvEntry::Var{access: _, ty}) => {
        let (e, (_, ty2)) = self.trans_exp(tm)?;
        check_types(&Box::new(ty.clone()), &ty2)?;
        let (_, access) = e.level.alloc_local();
        let venv = e.venv.insert(n, Box::new(EnvEntry::Var { access, ty: ty.clone() }));
        Ok(e.set_venv(venv))
      }
      _ => Err(format!("Undefined type: {:?}", ty))
    }
  }

  fn trans_bin(&self, op: &BinOp, l: &Box<ast::Exp<'a>>, r: &Box<ast::Exp<'a>>, _: &Pos)
      -> Result<(Env<'a>, ExpTy<'a>), String> {
    match op {
      BinOp::Add | BinOp::Sub | BinOp::Mul | BinOp::Div => {
        let (e, tm1, tm2) = self.check_both_ints(l, r)?;
        let mut trans = e.trans.clone();
        let tm1 = trans.un_ex(tm1);
        let tm2 = trans.un_ex(tm2);
        Ok((e.set_trans(trans), (translate::bin_op_(op, tm1, tm2), INT)))
      }
      BinOp::Le | BinOp::Lt | BinOp::Ge | BinOp::Gt => {
        let (e, tm1, tm2) = self.check_both_ints(l, r)?;
        let mut trans = e.trans.clone();
        let tm1 = trans.un_ex(tm1);
        let tm2 = trans.un_ex(tm2);
        let tm = trans.rel_op_(&op, tm1, tm2);
        Ok((e.set_trans(trans), (tm, BOOL)))
      }
      BinOp::And | BinOp::Or => {
        let (e, tm1, tm2) = self.check_both_bools(l, r)?;
        let mut trans = e.trans.clone();
        let tm1 = trans.un_cx(tm1);
        let tm2 = trans.un_cx(tm2);
        let tm = trans.rel_op_cx(op, tm1, tm2);
        Ok((e.set_trans(trans), (tm, BOOL)))
      }
      BinOp::Equ => {
        let (e, tm1, tm2) = self.check_type_equiv(l, r)?;
        let mut trans = e.trans.clone();
        let tm1 = trans.un_ex(tm1);
        let tm2 = trans.un_ex(tm2);
        let tm = trans.equ(tm1, tm2);
        Ok((e.set_trans(trans), (tm, BOOL)))
      }
    }
  }

  fn trans_call(&self, n: &'a str, vs: &Vec<Box<ast::Exp<'a>>>)
      -> Result<(Env<'a>, ExpTy<'a>), String> {
    match self.venv.get(n).map(|b| b.as_ref()) {
      Some(EnvEntry::Fun{level: _, label, formals, result}) => {
        if formals.len() != vs.len() {
          return Err(format!("Count of parameters mismatch: expected = {}, actual = {}", formals.len(), vs.len()))
        }
        let mut exs = Vec::new();
        let mut env = self.clone();
        for (v, ty1) in vs.into_iter().zip(formals.into_iter()) {
          let (env1, (ex, ty2)) = env.trans_exp(v)?;
          if ty1 != &ty2 {
            return Err(format!("Type mismatch: expected = {:?}, actual = {:?}", ty1, ty2))
          }
          exs.push(env.trans.un_ex(ex));
          env = env1;
        }
        Ok((env, (exp(call(label.clone(), exs)), result.clone())))
      }
      _ => return Err(format!("No function: {}", n))
    }
  }

  fn trans_var(&self, v: &Box<Var<'a>>) -> Result<(Env<'a>, ExpTy<'a>), String> {
    match v.as_ref() {
      Var::Simple(n) => {
        let ee = self.venv.get(n).ok_or(format!("Undefined value: {}", n))?;
        match ee.as_ref() {
          EnvEntry::Var{access, ty} => {
            let ir = simple_var(access, self.level.clone())?;
            Ok((self.clone(), (ir, ty.clone())))
          },
          _ => Err(format!("No variable: {}", n))
        }
      },
      Var::Field(r, f) => {
        let (e, (r, ty)) = self.trans_var(r)?;
        match ty {
          Type::Record(fs, _) => {
            let i = fs.iter().position(|x| &x.0 == f).ok_or(format!("No field: {}", f))?;
            let mut trans = e.trans.clone();
            let r = trans.un_ex(r);
            let exp = translate::field(r, i)?;
            let (ty, _) = find_type_and_size(fs[i].1, &e.tenv)?;
            Ok((e.set_trans(trans), (exp, ty)))
          }
          _ => Err(format!("Not a record type: {:?}", v))
        }
      }
      Var::Subscript(a, i) => {
        let (e, (a, ty)) = self.trans_var(a)?;
        match ty {
          Type::Array(_, _) => {
            let (e, (i, ty)) = e.trans_exp(i)?;
            let mut trans = e.trans.clone();
            let a = trans.un_ex(a);
            let i = trans.un_ex(i);
            let exp = translate::subscript(a, i)?;
            Ok((e.set_trans(trans), (exp, ty)))
          }
          _ => Err(format!("Not an array type: {:?}", v))
        }
      }
    }
  }

  fn trans_record(&self, fs: &Vec<(&'a str, Box<ast::Exp<'a>>)>, c: &'a str) -> Result<(Env<'a>, ExpTy<'a>), String> {
    let (ty, _) = find_type_and_size(c, &self.tenv)?;
    let mut ss = Vec::new();
    let mut e = self.clone();
    for f in fs.iter() {
      let (e1, (tm, _)) = e.trans_exp(&f.1)?;
      ss.push(tm);
      e = e1;
    }
    let mut trans = e.trans.clone();
    let tm = trans.trans_record(ss);
    Ok((e, (tm, ty)))
  }

  fn trans_let(&self, ds: &Vec<Box<Dec<'a>>>, exp: &Box<ast::Exp<'a>>) -> Result<(Env<'a>, ExpTy<'a>), String> {
    let e = self.trans_decs(ds)?;
    let (e, expty) = e.trans_exp(exp)?;
    Ok((self.set_trans(e.trans), expty))
  }

  fn trans_int(&self, i: &i32) -> Result<(Env<'a>, ExpTy<'a>), String> {
    Ok((self.clone(), (exp(cnst(i.clone())), INT)))
  }

  fn trans_str(&self, s: &'a str) -> Result<(Env<'a>, ExpTy<'a>), String> {
    let mut trans = self.trans.clone();
    let exp = trans.trans_str(s);
    Ok((self.set_trans(trans), (exp, STRING)))
  }

  fn check_both_ints(&self, tm1: &Box<ast::Exp<'a>>, tm2: &Box<ast::Exp<'a>>)
      -> Result<(Env<'a>, Box<translate::Exp<'a>>, Box<translate::Exp<'a>>), String> {
    let (e, (tm1, ty1)) = self.trans_exp(tm1)?;
    let (e, (tm2, ty2)) = e.trans_exp(tm2)?;
    check_types(&ty1, &INT)?;
    check_types(&ty2, &INT)?;
    Ok((e, tm1, tm2))
  }

  fn check_both_bools(&self, tm1: &Box<ast::Exp<'a>>, tm2: &Box<ast::Exp<'a>>)
      -> Result<(Env<'a>, Box<translate::Exp<'a>>, Box<translate::Exp<'a>>), String> {
    let (e, (tm1, ty1)) = self.trans_exp(tm1)?;
    let (e, (tm2, ty2)) = e.trans_exp(tm2)?;
    check_types(&ty1, &BOOL)?;
    check_types(&ty2, &BOOL)?;
    Ok((e, tm1, tm2))
  }

  fn check_type_equiv(&self, tm1: &Box<ast::Exp<'a>>, tm2: &Box<ast::Exp<'a>>)
      -> Result<(Env<'a>, Box<translate::Exp<'a>>, Box<translate::Exp<'a>>), String> {
    let (e, (tm1, ty1)) = self.trans_exp(tm1)?;
    let (e, (tm2, ty2)) = e.trans_exp(tm2)?;
    check_types(&ty1, &ty2)?;
    Ok((e, tm1, tm2))
  }

  fn set_venv(&self, venv: VEnv<'a>) -> Env<'a> {
    Env { tenv: self.tenv.clone(), venv, level: self.level.clone(), trans: self.trans.clone() }
  }

  fn set_tenv(&self, tenv: TEnv<'a>) -> Env<'a> {
    Env { tenv, venv: self.venv.clone(), level: self.level.clone(), trans: self.trans.clone() }
  }

  fn set_level(&self, level: Rc<Level<'a>>) -> Env<'a> {
    Env { tenv: self.tenv.clone(), venv: self.venv.clone(), level, trans: self.trans.clone() }
  }

  fn set_trans(&self, trans: Translator<'a>) -> Env<'a> {
    Env { tenv: self.tenv.clone(), venv: self.venv.clone(), level: self.level.clone(), trans }
  }
}

fn check_types<'a>(t1: &Type<'a>, t2: &Type<'a>) -> Result<(), String> {
  if t1 == t2 {
    Ok(())
  } else {
    Err(format!("Type mismatch: {:?} {:?}", t1, t2))
  }
}

fn find_type_and_size<'a>(i: &'a str, tenv: &TEnv<'a>) -> Result<(Type<'a>, usize), String> {
  let ty = tenv.get(i).ok_or_else(|| format!("No type: {}", i))?;
  match ty {
    Type::Name(i, _) => find_type_and_size(i, tenv),
    Type::Array(_, _) => Ok((ty.clone(), translate::WORD_SIZE)),
    Type::Record(fs, _) => {
      let mut sum = 0;
      for f in fs.into_iter() {
        let ast::Field(_, i) = f.as_ref();
        let (_, s) = find_type_and_size(i, tenv)?;
        sum += s;
      }
      Ok((ty.clone(), sum))
    }
  }
}

fn vector1<T>(t: T) -> Vec<T> {
  let mut v = Vec::new();
  v.push(t);
  v
}
