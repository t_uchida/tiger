#[macro_use] extern crate lalrpop_util;
extern crate rpds;

lalrpop_mod!(pub tiger);

pub mod ast;
pub mod frame;
pub mod ir;
pub mod temp;
pub mod translate;
pub mod semant;

use std::io::{self, Read};

fn main() {
  let src = {
    let mut src = String::new();
    let stdin = io::stdin();
    let mut handle = stdin.lock();
    let _ = handle.read_to_string(&mut src);
    src
  };

  let p = tiger::exprParser::new().parse(&src);
  match p {
    Ok(s) => {
      println!("{:?}", s);
      let progs = semant::run(&s);
      match progs {
        Ok(progs) =>
          for prog in progs.into_iter() {
            match prog {
              translate::Frag::Str(l, s) => println!("{} \"{}\"", l.format(), s),
              translate::Frag::Proc(s, f) => println!(":{}\n{}", f.name(), s.format()),
            }
          },
        Err(e) => println!("{}", e),
      }
    },
    Err(e) => println!("{:?}", e),
  }
}
