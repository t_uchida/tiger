use std::str::FromStr;
use ast::{Pos, Var, Exp, Dec, Type, UnOp, BinOp, Field};

grammar;

TierL<Op, NextTier>: Box<Exp<'input>> = {
  <l: @L> <t1:TierL<Op, NextTier>> <op:Op> <t2:NextTier> <r: @R> => Box::new(Exp::Bin(op, t1, t2, (l, r))),
  NextTier,
};

TierR<Op, NextTier>: Box<Exp<'input>> = {
  <l: @L> <t1:NextTier> <op:Op> <t2:TierR<Op, NextTier>> <r: @R> => Box::new(Exp::Bin(op, t1, t2, (l, r))),
  NextTier,
};

List<Sep, T>: Vec<T> = {
  <v: (<T> Sep)*> <e: T?> => match e {
    None => v,
    Some(e) => {
      let mut v = v;
      v.push(e);
      v
    }
  }
};

pub expr: Box<Exp<'input>> = {
  let_expr,
  if_expr,
  while_expr,
  for_expr,
  break_expr,
  term,
};

let_expr: Box<Exp<'input>> = {
  <l: @L> "let" <ds:dec_list> "in" <e:expr> "end" <r: @R>
    => Box::new(Exp::Let(ds, e, (l, r))),
};

if_expr: Box<Exp<'input>> = {
  <l: @L> "if" <c:term> "then" <e1:expr> "else" <e2: expr> <r: @R>
    => Box::new(Exp::If(c, e1, e2, (l, r))),
}

while_expr: Box<Exp<'input>> = {
  <l: @L> "while" <c:term> "do" <e:expr> <r: @R>
    => Box::new(Exp::While(c, e, (l, r))),
}

for_expr: Box<Exp<'input>> = {
  <l: @L> "for" <i:ident> ":=" <t1:term> "to" <t2:term> "do" <e:expr> <r: @R>
    => Box::new(Exp::For(i, t1, t2, e, (l, r))),
}

break_expr: Box<Exp<'input>> = {
  <l: @L> "break" <r: @R> => Box::new(Exp::Break((l, r))),
}

str_expr: Box<Exp<'input>> = {
  <l: @L> <s: string> <r: @R> => Box::new(Exp::Str(s, (l, r))),
}

dec_list: Vec<Box<Dec<'input>>> = List<semicolon, dec>;

dec: Box<Dec<'input>> = {
  variable_dec,
  function_dec,
  type_dec,
};

variable_dec: Box<Dec<'input>> = {
  <l: @L> "var" <i:ident> <ty: variable_dec_type?> ":=" <tm:term> <r: @R>
    => Box::new(Dec::Var(i, ty, tm, (l, r))),
};

variable_dec_type: (&'input str, Pos) = {
  ":" <l: @L> <i: ident> <r: @R> => (i, (l, r))
}

function_dec: Box<Dec<'input>> = {
  <l: @L> "function" <i:ident> "(" <ps:argument_list> ")" <ty: function_dec_type?> "=" <e:expr> <r: @R>
    => Box::new(Dec::Fun(i, ps, ty, e, (l, r))),
};

function_dec_type: (&'input str, Pos) = {
  ":" <l: @L> <i: ident> <r: @R> => (i, (l, r))
}

argument_list: Vec<Field<'input>> = List<comma, argument>;

argument: Field<'input> = {
  <v:ident> ":" <t:ident> => Field(v, t),
};

type_dec: Box<Dec<'input>> = {
  <l: @L> "type" <v:ident> "=" <t:type_instance> <r: @R> => Box::new(Dec::Type(v, t, (l, r))),
};

type_instance: Type<'input> = {
  <l: @L> "array" "of" <i:ident> <r: @R> => Type::Array(i, (l, r)),
  <l: @L> "{" <f:field_list> "}" <r: @R> => Type::Record(f, (l, r)),
  <l: @L> <i: ident> <r: @R> => Type::Name(i, (l, r)),
};

field_list = List<comma, field>;

field: Box<Field<'input>> = {
  <n:ident> ":" <t:ident> => Box::new(Field(n, t)),
}

expr_list: Vec<Box<Exp<'input>>> = {
  "(" <l:List<semicolon, expr>> ")" => l,
};

comma: () = ",";
semicolon: () = ";";

assignment: Box<Exp<'input>> = {
  <l: @L> <v:value> ":=" <t:term> <r: @R> => Box::new(Exp::Assign(v, t, (l, r)))
}

pub term: Box<Exp<'input>> = {
  binary_term,
};

binary_term: Box<Exp<'input>> = disjunction;

disjunction: Box<Exp<'input>> = TierL<disj_op, conjunction>;
conjunction: Box<Exp<'input>> = TierL<conj_op, equality>;
equality: Box<Exp<'input>> = TierL<eq_op, inequality>;
inequality: Box<Exp<'input>> = TierL<ineq_op, add_sub>;
add_sub: Box<Exp<'input>> = TierL<add_sub_op, mul_div>;
mul_div: Box<Exp<'input>> = TierL<mul_div_op, minus>;

minus: Box<Exp<'input>> = {
  <l: @L> "-" <f:fcall> <r: @R> => Box::new(Exp::Un(UnOp::Minus, f, (l, r))),
  fcall,
};

fcall: Box<Exp<'input>> = {
  <l: @L> <i:ident> "(" <v:term_list> ")" <r: @R> => Box::new(Exp::Call(i, v, (l, r))),
  <l: @L> <i:ident> "{" <fs: record_field_list> "}" <r: @R> => Box::new(Exp::Record(fs, i, (l, r))),
  str_expr,
  value => Box::new(Exp::Var(<>)),
  num => Box::new(Exp::Int(<>)),
}

record_field_list: Vec<(&'input str, Box<Exp<'input>>)> = List<comma, record_field>;

record_field: (&'input str, Box<Exp<'input>>) = {
  <i: ident> "=" <tm: term> => (i, tm)
}

value: Box<Var<'input>> = {
  <v:value> "[" <tm: term> "]" => Box::new(Var::Subscript(v, tm)),
  <v:value> "." <i:ident> => Box::new(Var::Field(v, i)),
  ident => Box::new(Var::Simple(<>)),
};

term_list: Vec<Box<Exp<'input>>> = List<comma, term>;

minus_op: UnOp = {
  "-" => UnOp::Minus,
};

mul_div_op: BinOp = {
  "*" => BinOp::Mul,
  "/" => BinOp::Div,
};

add_sub_op: BinOp = {
  "+" => BinOp::Add,
  "-" => BinOp::Sub,
};

ineq_op: BinOp = {
  "<" => BinOp::Lt,
  "<=" => BinOp::Le,
  ">" => BinOp::Gt,
  ">=" => BinOp::Ge,
};

eq_op: BinOp = {
  "=" => BinOp::Equ,
};

conj_op: BinOp = {
  "&" => BinOp::And,
};

disj_op: BinOp = {
  "|" => BinOp::Or,
};

string: &'input str = {
  <s: r"\x22[^\x22]*\x22"> => &s[1..(s.len()-1)]
}

num: i32 = {
  r"[0-9]+" => i32::from_str(<>).unwrap()
};

ident: &'input str = {
  r"[a-zA-Z_][a-zA-Z0-9_]*" => <>,
}
