#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Temp {
  id: u32,
}

pub const FP: Temp = Temp { id: 0 };
pub const SP: Temp = Temp { id: 1 };
pub const RV: Temp = Temp { id: 2 };

impl Temp {
  pub fn format(&self) -> String {
    match self.id {
      0 => String::from("FP"),
      1 => String::from("SP"),
      2 => String::from("RV"),
      _ => format!("#{}", self.id),
    }
  }
}

#[derive(Debug, Clone)]
pub struct TempMaker {
  next: u32,
}

impl TempMaker {
  pub fn new() -> TempMaker {
    TempMaker { next: 10 }
  }

  pub fn create(&mut self) -> Temp {
    let id = self.next;
    self.next += 1;
    Temp { id }
  }
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Label<'a> {
  id: u32,
  name: Option<&'a str>,
}

impl<'a> Label<'a> {
  pub fn format(&self) -> String {
    match self.name {
      Some(n) => format!(":{}", n),
      None => format!(":{}", self.id),
    }
  }
}

#[derive(Debug, Clone)]
pub struct LabelMaker {
  next: u32,
}


impl<'a> LabelMaker {
  pub fn new() -> LabelMaker {
    LabelMaker { next: 10 }
  }

  pub fn create(&mut self) -> Label<'a> {
    let id = self.next;
    self.next += 1;
    Label { id, name: None }
  }

  pub fn create_named(&mut self, name: &'a str) -> Label<'a> {
    let id = self.next;
    self.next += 1;
    Label { id, name: Some(name) }
  }
}

pub fn malloc<'a>() -> Label<'a> {
  Label { id: 0, name: Some("malloc") }
}