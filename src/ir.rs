use temp;

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Exp<'a> {
  Const(i32),
  Name(temp::Label<'a>),
  Temp(temp::Temp),
  BinOp(BinOp, Box<Exp<'a>>, Box<Exp<'a>>),
  Mem(Box<Exp<'a>>),
  Call(Box<Exp<'a>>, Vec<Box<Exp<'a>>>),
  ESeq(Box<Stm<'a>>, Box<Exp<'a>>),
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Stm<'a> {
  Move(Box<Exp<'a>>, Box<Exp<'a>>),
  Exp(Box<Exp<'a>>),
  Jump(Box<Exp<'a>>, Vec<temp::Label<'a>>),
  CJump(RelOp, Box<Exp<'a>>, Box<Exp<'a>>, temp::Label<'a>, temp::Label<'a>),
  Seq(Box<Stm<'a>>, Box<Stm<'a>>),
  Label(temp::Label<'a>),
  Nop,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum BinOp {
  Mul, Div, Add, Sub, And, Or, LShift, RShift, ARShift, Xor,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum RelOp {
  Equ, NEqu, Lt, Gt, Le, Ge, ULt, ULe, UGt, UGe,
}

impl<'a> Exp<'a> {
  fn format(&self) -> String {
    match self {
      Exp::Const(i) => i.to_string(),
      Exp::Name(l) => l.format(),
      Exp::Temp(t) => t.format(),
      Exp::BinOp(op, e1, e2) => format!("{:?}({}, {})", op, e1.format(), e2.format()),
      Exp::Mem(e) => format!("mem({})", e.format()),
      Exp::Call(e, ps) => format!("{}{}", e.format(), format_vec_exp(ps)),
      Exp::ESeq(s, e) => format!("{}\n{}", s.format(), e.format()),
    }
  }
}

fn format_vec_exp<'a>(ps: &Vec<Box<Exp<'a>>>) -> String {
  let mut s = String::from("(");
  for i in 0..ps.len() {
    s.push_str(&ps[i].format());
    if i > 0 {
      s.push_str(", ")
    }
  }
  s.push_str(")");
  s
}

impl<'a> Stm<'a> {
  pub fn format(&self) -> String {
    match self {
      Stm::Move(e1, e2) => format!("move {} {}", e1.format(), e2.format()),
      Stm::Exp(e) => e.format(),
      Stm::Jump(e, _) => format!("jump {}", e.format()),
      Stm::CJump(op, e1, e2, l1, l2) => format!("cjump {:?} {} {} {} {}", op, e1.format(), e2.format(), l1.format(), l2.format()),
      Stm::Seq(s1, s2) => format!("{}\n{}", s1.format(), s2.format()),
      Stm::Label(l) => l.format(),
      Stm::Nop => String::from("Nop"),
    }
  }
}