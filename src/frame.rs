use rpds::Vector;

#[derive(Clone, Debug)]
pub struct Frame<'a> {
  name: &'a str,
  count: i32,
  formals: Vector<Access>,
}

#[derive(Clone, Debug)]
pub struct Access {
  index: i32,
}

impl<'a> Frame<'a> {
  pub fn new(name: &'a str, num_formals: usize) -> Frame<'a> {
    let formals: Vector<_> = (0..num_formals).map(|i| Access { index: -(i as i32) }).collect();
    Frame { name, count: 0, formals }
  }

  pub fn outermost() -> Frame<'a> {
    Frame { name: "(outermost)", count: 0, formals: Vector::new() }
  }

  pub fn alloc_local(&self) -> (Frame, Access) {
    let count = self.count;
    (Frame {name: self.name, count: self.count + 1, formals: self.formals.clone() }, Access { index: count })
  }

  pub fn formals(&self) -> &Vector<Access> {
    &self.formals
  }

  pub fn name(&self) -> &'a str {
    self.name
  }
}

impl Access {
  pub fn get_offset(&self) -> i32 {
    self.index
  }
}