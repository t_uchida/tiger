use std::rc::Rc;

use rpds::Vector;

use ast;
use frame;
use ir;
use temp;

pub const WORD_SIZE: usize = 4;

type GenStm<'a> = Box<Fn(temp::Label<'a>, temp::Label<'a>) -> Box<ir::Stm<'a>> + 'a>;

pub enum Exp<'a> {
  Ex(Box<ir::Exp<'a>>),
  Nx(Box<ir::Stm<'a>>),
  Cx(GenStm<'a>),
}

#[derive(Debug, Clone)]
pub enum Frag<'a> {
  Proc(Box<ir::Stm<'a>>, frame::Frame<'a>),
  Str(temp::Label<'a>, &'a str),
}

pub fn simple_var<'a>(acc: &Access, lv: Rc<Level<'a>>) -> Result<Box<Exp<'a>>, String> {
  if acc.depth > lv.depth {
    Err(format!("Access depth is larger than level depth: access_depth = {}, level_depth = {}", acc.depth, lv.depth))
  } else {
    let mut exp = temp(temp::FP);
    let depth = lv.depth - acc.depth;
    for _ in 0..depth {
      exp = mem(exp)
    }
    Ok(Box::new(Exp::Ex(mem_plus(exp, cnst(acc.get_offset())))))
  }
}

pub fn field<'a>(v: Box<ir::Exp<'a>>, i: usize) -> Result<Box<Exp<'a>>, String> {
  Ok(Box::new(Exp::Ex(mem_plus(v, cnst((i * WORD_SIZE) as i32)))))
}

pub fn subscript<'a>(a: Box<ir::Exp<'a>>, i: Box<ir::Exp<'a>>) -> Result<Box<Exp<'a>>, String> {
  Ok(Box::new(Exp::Ex(mem_plus(a, i))))
}

pub fn call<'a>(label: temp::Label<'a>, ps: Vec<Box<ir::Exp<'a>>>) -> Box<ir::Exp<'a>> {
  Box::new(ir::Exp::Call(Box::new(ir::Exp::Name(label)), ps))
}

pub fn trans_break<'a>(lv: Rc<Level<'a>>) -> Result<(Box<ir::Stm<'a>>, Rc<Level<'a>>), String> {
  match lv.parent {
    Some(ref lv) => {
      let fp = temp(temp::FP);
      let sp = temp(temp::SP);
      Ok((seq(mov(sp, fp.clone()), mov(fp.clone(), mem(fp))), lv.clone()))
    }
    None => Err(format!("break occurred in outermost level."))
  }
}

pub fn trans_let<'a>(e: Box<ir::Exp<'a>>) -> Box<ir::Stm<'a>> {
  Box::new(ir::Stm::Exp(e))
}

#[derive(Clone, Debug)]
pub struct Translator<'a> {
  temp_maker: temp::TempMaker,
  label_maker: temp::LabelMaker,
  frags: Vector<Frag<'a>>,
}

impl<'a> Translator<'a> {
  pub fn new() -> Translator<'a> {
    Translator { temp_maker: temp::TempMaker::new(), label_maker: temp::LabelMaker::new(), frags: Vector::new() }
  }

  pub fn record_proc(&mut self, proc: Box<ir::Stm<'a>>, level: Rc<Level<'a>>) {
    self.frags = self.frags.push_back(Frag::Proc(proc, level.frame.clone()));
  }

  pub fn trans_assign(&mut self, l: Box<Exp<'a>>, r: Box<Exp<'a>>) -> Box<ir::Stm<'a>> {
    mov(self.un_ex(l), self.un_ex(r))
  }

  pub fn trans_if_then_else(&mut self, c: Box<Exp<'a>>, st: Box<ir::Stm<'a>>, sf: Box<ir::Stm<'a>>)
      -> Box<ir::Stm<'a>> {
    let c = self.un_cx(c);
    let t = self.label_maker.create();
    let f = self.label_maker.create();
    seq(
      c(t.clone(), f.clone()), seq(
      label(t), seq(
      st, seq(
      label(f),
      sf))))
  }

  pub fn trans_fun(&mut self, n: &'a str, e: Box<Exp<'a>>, f: frame::Frame<'a>) -> temp::Label<'a> {
    let l = self.label_maker.create_named(n);
    let s = seq(label(l.clone()), mov(temp(temp::RV), self.un_ex(e)));
    self.frags = self.frags.push_back(Frag::Proc(s, f));
    l
  }

  pub fn trans_str(&mut self, s: &'a str) -> Box<Exp<'a>> {
    let l = self.create_label();
    self.frags = self.frags.push_back(Frag::Str(l.clone(), s));
    Box::new(Exp::Ex(Box::new(ir::Exp::Name(l))))
  }

  pub fn trans_record(&mut self, fs: Vec<Box<Exp<'a>>>) -> Box<Exp<'a>> {
    let r = self.temp_maker.create();
    let m = mov(temp(r.clone()), self.un_ex(malloc(cnst((fs.len() * WORD_SIZE) as i32))));
    let mut n = fs.len() - 1;
    let mut iter = fs.into_iter().rev();
    match iter.next() {
      Some(s) => {
        let mut s = mov(mem_plus(temp(r.clone()), cnst((n * WORD_SIZE) as i32)), self.un_ex(s));
        for is in iter {
          n = n - 1;
          let s1 = mov(mem_plus(temp(r.clone()), cnst((n * WORD_SIZE) as i32)), self.un_ex(is));
          s = seq(s1, s)
        }
        Box::new(Exp::Ex(eseq(seq(m, s), temp(r))))
      }
      None => panic!("No field")
    }
  }

  pub fn trans_for(&mut self, l: Rc<Level<'a>>, a: Access, beg: Box<Exp<'a>>, end: Box<Exp<'a>>, s: Box<ir::Stm<'a>>)
      -> Result<Box<ir::Stm<'a>>, String> {
    let i = simple_var(&a, l)?;
    let i = self.un_ex(i);
    let limit = self.temp_maker.create();
    let cmp = self.label_maker.create();
    let lp = self.label_maker.create();
    let done = self.label_maker.create();
    Ok(seq(
      mov(i.clone(), self.un_ex(beg)), seq(
      mov(temp(limit.clone()), self.un_ex(end)), seq(
      label(cmp.clone()), seq(
      le(i.clone(), temp(limit), lp.clone(), done.clone()), seq(
      label(lp), seq(
      s, seq(
      mov(i.clone(), plus(i, cnst(1))), seq(
      jump(cmp),
      label(done))))))))))
  }

  pub fn rel_op_(&mut self, op: &ast::BinOp, l: Box<ir::Exp<'a>>, r: Box<ir::Exp<'a>>) -> Box<Exp<'a>> {
    let op = to_ir_rel_op(op);
    Box::new(Exp::Cx(Box::new(move |t, f| rel_op(&op, &l, &r, t, f))))
  }

  pub fn rel_op_cx(&mut self, op: &ast::BinOp, l: GenStm<'a>, r: GenStm<'a>) -> Box<Exp<'a>> {
    let z = self.label_maker.create();
    match op {
      ast::BinOp::And => Box::new(Exp::Cx(Box::new(move |t, f| seq(
        l(z.clone(), f.clone()), seq(
        label(z.clone()),
        r(t, f)))))),
      ast::BinOp::Or => Box::new(Exp::Cx(Box::new(move |t, f| seq(
        l(t.clone(), z.clone()), seq(
        label(z.clone()),
        r(t, f)))))),
      _ => panic!("Not supported"),
    }
  }

  pub fn equ(&mut self, l: Box<ir::Exp<'a>>, r: Box<ir::Exp<'a>>) -> Box<Exp<'a>> {
    Box::new(Exp::Cx(Box::new(move |t, f| rel_op(&ir::RelOp::Equ, &l, &r, t, f))))
  }

  pub fn return_value(&mut self, e: Box<Exp<'a>>, l: Rc<Level<'a>>) {
    let s = mov(temp(temp::RV), self.un_ex(e));
    self.frags = self.frags.push_back(Frag::Proc(s, l.frame.clone()));
  }

  pub fn un_ex(&mut self, e: Box<Exp<'a>>) -> Box<ir::Exp<'a>> {
    match *e {
      Exp::Ex(e) => Box::new((*e).clone()),
      Exp::Nx(s) => eseq(Box::new((*s).clone()), cnst(0)),
      Exp::Cx(genstm) => {
        let r = self.temp_maker.create();
        let t = self.label_maker.create();
        let f = self.label_maker.create();
        eseq(seq(
          mov(temp(r.clone()), cnst(1)), seq(
          genstm(t.clone(), f.clone()), seq(
          label(f), seq(
          mov(temp(r.clone()), cnst(0)),
          label(t))))),
          temp(r))
      },
    }
  }

  pub fn un_nx(&mut self, e: Box<Exp<'a>>) -> Box<ir::Stm<'a>> {
    match *e {
      Exp::Ex(e) => Box::new(ir::Stm::Exp(e)),
      Exp::Nx(s) => s,
      Exp::Cx(_) => panic!("Exp::Cx in Translator::un_nx()"),
    }
  }

  pub fn un_cx(&mut self, e: Box<Exp<'a>>) -> GenStm<'a> {
    match *e {
      Exp::Ex(e) => {
        let z = cnst(0);
        Box::new(move |t, f| rel_op(&ir::RelOp::NEqu, &e, &z, t, f))
      }
      Exp::Nx(s) => Box::new(move |t, f| seq(label(t), seq(label(f), (&s).clone()))),
      Exp::Cx(f) => f,
    }
  }

  pub fn create_named_label(&mut self, n: &'a str) -> temp::Label<'a> {
    self.label_maker.create_named(n)
  }

  pub fn create_label(&mut self) -> temp::Label<'a> {
    self.label_maker.create()
  }

  pub fn get_fragments(&self) -> &Vector<Frag<'a>> {
    &self.frags
  }
}

pub fn malloc<'a>(size: Box<ir::Exp<'a>>) -> Box<Exp<'a>> {
  let mut ps = Vec::new();
  ps.push(size);
  let malloc = temp::malloc();
  exp(call(malloc, ps))
}

pub fn to_ir_bin_op(op: &ast::BinOp) -> ir::BinOp {
  match op {
    ast::BinOp::Add => ir::BinOp::Add,
    ast::BinOp::Sub => ir::BinOp::Sub,
    ast::BinOp::Mul => ir::BinOp::Mul,
    ast::BinOp::Div => ir::BinOp::Div,
    ast::BinOp::And => ir::BinOp::And,
    ast::BinOp::Or => ir::BinOp::Or,
    _ => panic!("Not supported"),
  }
}

fn to_ir_rel_op(op: &ast::BinOp) -> ir::RelOp {
  match op {
    ast::BinOp::Equ => ir::RelOp::Equ,
    ast::BinOp::Gt => ir::RelOp::Gt,
    ast::BinOp::Ge => ir::RelOp::Ge,
    ast::BinOp::Lt => ir::RelOp::Lt,
    ast::BinOp::Le => ir::RelOp::Le,
    _ => panic!("Not supported"),
  }
}

pub fn bin_op<'a>(op: ir::BinOp, l: Box<ir::Exp<'a>>, r: Box<ir::Exp<'a>>) -> Box<ir::Exp<'a>> {
  Box::new(ir::Exp::BinOp(op, l, r))
}

pub fn bin_op_<'a>(op: &ast::BinOp, l: Box<ir::Exp<'a>>, r: Box<ir::Exp<'a>>) -> Box<Exp<'a>> {
  Box::new(Exp::Ex(Box::new(ir::Exp::BinOp(to_ir_bin_op(op), l, r))))
}

fn rel_op<'a>(op: &ir::RelOp, l: &Box<ir::Exp<'a>>, r: &Box<ir::Exp<'a>>, t: temp::Label<'a>, f: temp::Label<'a>)
    -> Box<ir::Stm<'a>> {
  Box::new(ir::Stm::CJump(op.clone(), l.clone(), r.clone(), t, f))
}

pub fn eseq<'a>(s: Box<ir::Stm<'a>>, e: Box<ir::Exp<'a>>) -> Box<ir::Exp<'a>> {
  Box::new(ir::Exp::ESeq(s, e))
}

pub fn eseq_<'a>(s: Box<ir::Stm<'a>>, e: Box<ir::Exp<'a>>) -> Box<Exp<'a>> {
  Box::new(Exp::Ex(Box::new(ir::Exp::ESeq(s, e))))
}

fn temp<'a>(t: temp::Temp) -> Box<ir::Exp<'a>> {
  Box::new(ir::Exp::Temp(t))
}

pub fn cnst<'a>(c: i32) -> Box<ir::Exp<'a>> {
  Box::new(ir::Exp::Const(c))
}

fn mov<'a>(e1: Box<ir::Exp<'a>>, e2: Box<ir::Exp<'a>>) -> Box<ir::Stm<'a>> {
  Box::new(ir::Stm::Move(e1, e2))
}

fn label<'a>(t: temp::Label<'a>) -> Box<ir::Stm<'a>> {
  Box::new(ir::Stm::Label(t))
}

fn seq<'a>(s1: Box<ir::Stm<'a>>, s2: Box<ir::Stm<'a>>) -> Box<ir::Stm<'a>> {
  Box::new(ir::Stm::Seq(s1, s2))
}

fn mem<'a>(e: Box<ir::Exp<'a>>) -> Box<ir::Exp<'a>> {
  Box::new(ir::Exp::Mem(e))
}

fn mem_plus<'a>(e1: Box<ir::Exp<'a>>, e2: Box<ir::Exp<'a>>) -> Box<ir::Exp<'a>> {
  Box::new(ir::Exp::Mem(plus(e1, e2)))
}

pub fn plus<'a>(e1: Box<ir::Exp<'a>>, e2: Box<ir::Exp<'a>>) -> Box<ir::Exp<'a>> {
  Box::new(ir::Exp::BinOp(ir::BinOp::Add, e1, e2))
}

pub fn mul<'a>(e1: Box<ir::Exp<'a>>, e2: Box<ir::Exp<'a>>) -> Box<ir::Exp<'a>> {
  Box::new(ir::Exp::BinOp(ir::BinOp::Mul, e1, e2))
}

fn le<'a>(e1: Box<ir::Exp<'a>>, e2: Box<ir::Exp<'a>>, lt: temp::Label<'a>, lf: temp::Label<'a>) -> Box<ir::Stm<'a>> {
  Box::new(ir::Stm::CJump(ir::RelOp::Le, e1, e2, lt, lf))
}

fn jump<'a>(l: temp::Label<'a>) -> Box<ir::Stm<'a>> {
  let mut labs = Vec::new();
  labs.push(l.clone());
  Box::new(ir::Stm::Jump(Box::new(ir::Exp::Name(l.clone())), labs))
}

pub fn exp<'a>(ex: Box<ir::Exp<'a>>) -> Box<Exp<'a>> {
  Box::new(Exp::Ex(ex))
}

pub fn stms<'a>(iss: Vec<Box<ir::Stm<'a>>>) -> Result<Box<ir::Stm<'a>>, String> {
  let mut iter = iss.into_iter().rev();
  match iter.next() {
    Some(mut s) => {
      for is in iter {
        s = Box::new(ir::Stm::Seq(is, s))
      }
      Ok(s)
    }
    None => Err(format!("stms(): No statements"))
  }
}

#[derive(Clone, Debug)]
pub struct Access {
  access: frame::Access,
  depth: usize,
}

#[derive(Debug)]
pub struct Level<'a> {
  frame: frame::Frame<'a>,
  parent: Option<Rc<Level<'a>>>,
  depth: usize,
}

impl Access {
  pub fn get_offset(&self) -> i32 {
    self.access.get_offset()
  }
}

impl<'a> Level<'a> {
  pub fn new(name: &'a str, num_formals: usize, parent: Rc<Level<'a>>) -> Rc<Level<'a>> {
    Rc::new(Level {
      frame: frame::Frame::new(name, num_formals + 1),
      parent: Some(parent.clone()),
      depth: parent.depth + 1,
    })
  }

  pub fn outermost() -> Rc<Level<'a>> {
    Rc::new(Level { frame: frame::Frame::outermost(), parent: None, depth: 0 })
  }

  pub fn formals(&self) -> &Vector<frame::Access> {
    // TODO Output &[1..]
    self.frame.formals()
  }

  pub fn alloc_local(&self) -> (Level, Access) {
    let (frame, access) = self.frame.alloc_local();
    (Level { frame, parent: self.parent.clone(), depth: self.depth}, Access { access, depth: self.depth })
  }

  pub fn get_frame(&self) -> frame::Frame<'a> {
    self.frame.clone()
  }
}
